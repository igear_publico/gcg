package idearium.gcg;

import static org.springframework.http.HttpStatus.OK;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Value("${logging.path}")
    String logpath;

    @Autowired
    Authentication auth;
    
    @Autowired
    GcgService gcg;
    @Autowired
    SolrService solr;
    public final static Logger log = LoggerFactory.getLogger("gcg");
    
    @RequestMapping(value = "/logs", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity logs() throws Exception {
    	try{
    		byte[] encoded = Files.readAllBytes(Paths.get(logpath+"spring.log"));
    		String s = new String(encoded, StandardCharsets.US_ASCII);
    		return ResponseEntity.status(OK).body(s);
    		}
    		catch(Exception ex){
    			log.error("Error obteniendo log",ex);
    			return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getLocalizedMessage());
    		}
    }

   
    
    @RequestMapping(value={"/load/fromDB"}, method = { RequestMethod.GET, RequestMethod.POST }, produces = "text/plain")
    public ResponseEntity loadFromDB(@RequestParam(value = "id",required = false) Integer id, @RequestParam(value = "us",required = true) String user,@RequestParam(value = "cl",required = true) String pwd){
    	
    	try{
    		if (!auth.validateUser(user, pwd)){
        		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario no válido");
        	}
    		if (id==null){
    			gcg.updateAll();		
    		}
    		else{
    			gcg.update(id);
    		}
    	
    	return ResponseEntity.status(OK).body("OK");
    	}
    	catch(Exception ex){
    		log.info("error", ex);
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error");
    	}
    	
    }
    
    @RequestMapping(value={"/load/fromFiles"}, method = { RequestMethod.GET, RequestMethod.POST }, produces = "text/plain")
    public ResponseEntity loadFromFiles(@RequestParam(value = "us",required = true) String user,@RequestParam(value = "cl",required = true) String pwd){
    	try{
    		if (!auth.validateUser(user, pwd)){
        		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario no válido");
        	}
    	gcg.loadFromFiles();
    	return ResponseEntity.status(OK).body("OK");
    	}
    	catch(Exception ex){
    		log.info("error", ex);
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error");
    	}
    	
    }
    
    @RequestMapping(value={"/reindex"}, method = { RequestMethod.GET, RequestMethod.POST }, produces = "text/plain")
    public ResponseEntity reindex(@RequestParam(value = "tipo",required = false) String tipo, @RequestParam(value = "us",required = true) String user,@RequestParam(value = "cl",required = true) String pwd){
    	try{
    		if (!auth.validateUser(user, pwd)){
        		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario no válido");
        	}
    		if (tipo==null){
    	solr.indexGraph();
    		}
    		else{
    			solr.indexType(tipo);
    		}
    	log.debug("Fin del proceso");
    	return ResponseEntity.status(OK).body("OK");
    	}
    	catch(Exception ex){
    		log.info("error", ex);
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error");
    	}
    	
    }
    
    @RequestMapping(value={"/delete"}, method = { RequestMethod.GET, RequestMethod.POST }, produces = "text/plain")
    public ResponseEntity delete(@RequestParam(value = "tipo",required = false) String tipo, @RequestParam(value = "us",required = true) String user,@RequestParam(value = "cl",required = true) String pwd){
    	try{
    		if (!auth.validateUser(user, pwd)){
        		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario no válido");
        	}
    		if (tipo==null){
    	gcg.delete();
    		}
    		else{
    			gcg.deleteType(tipo);
    		}
    	log.debug("Fin del proceso");
    	return ResponseEntity.status(OK).body("OK");
    	}
    	catch(Exception ex){
    		log.info("error", ex);
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error");
    	}
    	
    }
 }
