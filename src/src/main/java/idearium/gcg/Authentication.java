package idearium.gcg;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public  class Authentication {
	static private String AUTHENTICATION_IGEAR_URL="/gestorUsuarios/gestorUsuariosIGEAR?REQUEST=";

	static private String publicKey=;

    public final static Logger log = LoggerFactory.getLogger("gcg");
	static private String ROL_APP = "ROL_G_GCG";
    @Value("${authentication.server}") 
    private String server;

	 public boolean validateUser (String user,String pwd) throws Exception{
		 log.debug(server+AUTHENTICATION_IGEAR_URL+"LOGIN&us=" + user + "&cl=" +URLEncoder.encode(pwd));
		URL url = new URL(server+AUTHENTICATION_IGEAR_URL+"LOGIN&us=" + user + "&cl=" +URLEncoder.encode(pwd));
	
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();
		int code = connection.getResponseCode();
	
		
		log.debug("auth "+code);
		if( code==200){
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			String token =  rd.readLine();
			connection.disconnect();

			return hasGCGRol(token);
		}
		else{
			connection.disconnect();
			return false;
		}


	}
		
		protected  boolean hasGCGRol(String token) throws Exception{
			 log.debug(server+AUTHENTICATION_IGEAR_URL+"HASROL&rol="+ROL_APP+"&TOKEN="+token);
				URL url = new URL(server+AUTHENTICATION_IGEAR_URL+"HASROL&rol="+ROL_APP+"&TOKEN="+token);
			
				HttpURLConnection connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("GET");
				connection.setDoOutput(true);
				connection.connect();
				int code = connection.getResponseCode();
			
				connection.disconnect();
				log.debug("rol "+code);
				return code==200;
		
		}
}
