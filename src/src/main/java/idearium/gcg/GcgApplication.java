package idearium.gcg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
public class GcgApplication extends SpringBootServletInitializer{


	public static void main(String[] args) {
		SpringApplication.run(GcgApplication.class, args);
	}

}
