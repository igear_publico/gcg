package idearium.gcg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

@Service
public class DataSourceConfiguration {

    private HashMap<String, Connection> hash = new HashMap();

    @Autowired
    private Environment env;

    public DataSource getDataSource(String conex){
      /*  if(conex.equals("conexSpSS_GCG")){
            return getDataSourceGCG();
        }else if(conex.equals("conexSpSS_igar")){
            return getDataSourceIgar();
        }else if(conex.equals("conexSpSS_inga")){
            return getDataSourceInga();
        }else if(conex.equals("conexSpSS_tran")){
            return getDataSourceTran();
        }else if(conex.equals("conexSpSS_patc")){
            return getDataSourcePatc();
        }else if(conex.equals("conexSpSS_medn")){
            return getDataSourceMedn();
        }else if(conex.equals("conexSpSS_dplan")){
            return getDataSourceDplan();
        }else if(conex.equals("conexSpSS_slud")){
            return getDataSourceSlud();
        }else if(conex.equals("conexSpSS_virtuoso")){
            return getDataSourceVirtuoso();
        }else{
            return null;
        }*/
    	DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty(conex+".url"));
        dataSourceBuilder.username(env.getProperty(conex+".user"));
        dataSourceBuilder.password(env.getProperty(conex+".password"));
        return dataSourceBuilder.build();
    }

    public DataSource getDataSourceIgar() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("igar.url"));
        dataSourceBuilder.username(env.getProperty("igar.user"));
        dataSourceBuilder.password(env.getProperty("igar.password"));
        return dataSourceBuilder.build();
    }
    public DataSource getDataSourceInga() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("inga.url"));
        dataSourceBuilder.username(env.getProperty("inga.user"));
        dataSourceBuilder.password(env.getProperty("inga.password"));
        return dataSourceBuilder.build();
    }
    public DataSource getDataSourceTran() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("tran.url"));
        dataSourceBuilder.username(env.getProperty("tran.user"));
        dataSourceBuilder.password(env.getProperty("tran.password"));
        return dataSourceBuilder.build();
    }
    public DataSource getDataSourcePatc() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("patc.url"));
        dataSourceBuilder.username(env.getProperty("patc.user"));
        dataSourceBuilder.password(env.getProperty("patc.password"));
        return dataSourceBuilder.build();
    }
    public DataSource getDataSourceMedn() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("medn.url"));
        dataSourceBuilder.username(env.getProperty("medn.user"));
        dataSourceBuilder.password(env.getProperty("medn.password"));
        return dataSourceBuilder.build();
    }
    public DataSource getDataSourceDplan() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("dplan.url"));
        dataSourceBuilder.username(env.getProperty("dplan.user"));
        dataSourceBuilder.password(env.getProperty("dplan.password"));
        return dataSourceBuilder.build();
    }
    public DataSource getDataSourceSlud() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("slud.url"));
        dataSourceBuilder.username(env.getProperty("slud.user"));
        dataSourceBuilder.password(env.getProperty("slud.password"));
        return dataSourceBuilder.build();
    }

    public DataSource getDataSourceGCG() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.postgresql"));
        dataSourceBuilder.url(env.getProperty("gcg.url"));
        dataSourceBuilder.username(env.getProperty("gcg.user"));
        dataSourceBuilder.password(env.getProperty("gcg.password"));
        return dataSourceBuilder.build();
    }


    public DataSource getDataSourceVirtuoso() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("driver.virtuoso"));
        dataSourceBuilder.url(env.getProperty("virtuoso.url"));
        dataSourceBuilder.username(env.getProperty("virtuoso.user"));
        dataSourceBuilder.password(env.getProperty("virtuoso.password"));
        return dataSourceBuilder.build();
    }


    public Connection getConnection(String con) throws SQLException {

        Connection c = this.hash.get(con);
       if ((c!=null)&& !c.isValid(10)){
    	   try{
    	   c.close();
    	   }
    	   catch(Exception ex){
    		   
    	   }
       }
        if(c == null || c.isClosed()){
            DataSource d = getDataSource(con);
            c = d.getConnection();
            this.hash.put(con,c);
            return c;
        }else{
            return c;
        }
    }
    public void closeConnection(String con) {
    	 Connection c = this.hash.get(con);
        	try{
            c.close();
        	}
        	catch(Exception ex){
        		
        	}
        	this.hash.remove(con);
        
    }

    /*public void closeConnections() {
        for(Connection c :this.hash.values()){
        	try{
            c.close();
        	}
        	catch(Exception ex){
        		
        	}
        }
        hash = new HashMap();
    }*/
}
