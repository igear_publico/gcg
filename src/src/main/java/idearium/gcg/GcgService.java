package idearium.gcg;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RDFParserBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoUpdateFactory;
import virtuoso.jena.driver.VirtuosoUpdateRequest;

@Service
public class GcgService {

	public final static Logger log = LoggerFactory.getLogger("gcg");


	//VirtGraph graph;

	@Value("${virtuoso.url}")
	String url_virtuoso;
	@Value("${virtuoso.user}")
	String user_virtuoso;
	@Value("${virtuoso.password}")
	String pwd_virtuoso;
	@Value("${virtuoso.grafo}")
	String grafo_virtuoso;
	@Value("${sparql.url}")
	String url_sparql;
	@Autowired
	private DataSourceConfiguration data;

	public void loadFromFiles() throws Exception{
		try{


			Connection conGCG = data.getConnection("gcg");
			Statement stmt =conGCG.createStatement();
			ResultSet rs = stmt.executeQuery("select * from ficheros where esactivo order by orden");
			while (rs.next()){
				loadTtlFile(rs.getString("fichero_ttl"));
			}
		}
		finally{
			data.closeConnection("gcg");
		}

	}
	
	public void delete() throws Exception{
		try{


			Connection conGCG = data.getConnection("gcg");
			Statement stmt =conGCG.createStatement();
			ResultSet rs = stmt.executeQuery("select tipo from config_busquedas where borrar");
			while (rs.next()){
				deleteType(rs.getString("tipo"));
			}
		}
		finally{
			data.closeConnection("gcg");
		}

	}
	
	protected void updateGraph(String where)throws Exception{
		VirtGraph graph =null;
		try{
			Connection conGCG = data.getConnection("gcg");
			Statement stmt =conGCG.createStatement();
			ResultSet rs = stmt.executeQuery("select * from gcg where "+where+" order by orden");
			log.info(grafo_virtuoso);
			log.info(url_virtuoso);
			graph = new VirtGraph(grafo_virtuoso, url_virtuoso, user_virtuoso, pwd_virtuoso);
			
			while (rs.next()){
				try{
				InputStream in = new ClassPathResource("plantillas_ttl/"+rs.getString("plantilla_ttl")+".ttl").getInputStream();
				StringBuilder plantilla = new StringBuilder();
				try (BufferedReader br
						= new BufferedReader(new InputStreamReader(in))) {
					String line;
					while ((line = br.readLine()) != null) {
						plantilla.append(line).append("\n");
					}
				}
				String ttl = plantilla.toString();
				// Siguiente línea solo para Windows
				//ttl=(new String(ttl.getBytes("ISO-8859-1"), "UTF-8"));
				Connection connObj = data.getConnection(rs.getString("conexion_objeto"));
				Statement stmtObj =connObj.createStatement();
				String filtro = rs.getString("filtro");
				Boolean congeom = rs.getBoolean("congeom");
				log.info("select *"+(congeom?",st_asText(st_transform(shape,4326)) as wkt,replace(st_geometryType(shape),'ST_','') as wkt_type":"")+" from "+rs.getString("capa_objeto")+(filtro!=null && filtro.trim().length()>0?" WHERE "+filtro:""));
				ResultSet rs_datos = stmtObj.executeQuery("select *"+(congeom?",st_asText(st_transform(shape,4326)) as wkt,replace(st_geometryType(shape),'ST_','') as wkt_type":"")+" from "+rs.getString("capa_objeto")+
						(filtro!=null && filtro.trim().length()>0?" WHERE "+filtro:"")/*+" LIMIT 10"*/);
				ResultSetMetaData rsmd = rs_datos.getMetaData();
				while (rs_datos.next()){
					String ttl_object=ttl;
					for (int i=1; i<=rsmd.getColumnCount();i++){
						String column = rsmd.getColumnName(i);
						if (ttl.indexOf("<<"+column+">>")>=0){
							String value = rs_datos.getString(column);
							if (value != null){
								ttl_object = ttl_object.replaceAll("<<"+column+">>", value);
							}
							else{
								log.info("es nulo");
								ttl_object = cleanTtl(ttl_object, "<<"+column+">>");
								log.info("cleaned");
							}
						}
					}
					log.info("load ttl");
					loadTtl(graph,ttl_object);
				}
				rs_datos.close();
				stmtObj.close();
								
				updateRelEspaciales(graph,rs.getString("id"),rs.getString("capa_objeto"),rs.getString("conexion_objeto"),rs.getString("filtro"));
				Statement stmt_upd =conGCG.createStatement();
				stmt_upd.executeUpdate("update gcg set ultima_actualizacion=current_timestamp, error=null where id="+rs.getInt("id"));
				stmt_upd.close();
				}
				catch(Exception ex){
					Statement stmt_upd =conGCG.createStatement();
					stmt_upd.executeUpdate("update gcg set error='"+ex.getMessage()+"' where id="+rs.getInt("id"));
					stmt_upd.close();
					throw ex;
				}
			}
			rs.close();
			stmt.close();

		}
		finally{
		//	data.closeConnections();
			if (graph!=null){
			graph.close();
			}
			
		}
	}
	public void update(int id) throws Exception{
		updateGraph("id="+id);
	

	}
	public void updateAll() throws Exception{
		updateGraph("esactivo");
	

	}

	protected void updateRelEspaciales(VirtGraph graph,String id_sujeto, String capa_sujeto, String conexion_sujeto, String filtro_sujeto) throws Exception{
		
		Connection conGCG = data.getConnection("gcg");
		Statement stmt =conGCG.createStatement();
		log.info("select r.*,p.predicado as uri_predicado from relaciones_espaciales r inner join codpredicados p on r.predicado=p.id where esactivo and sujeto="+id_sujeto);
		ResultSet rs = stmt.executeQuery("select r.*,p.predicado as uri_predicado from relaciones_espaciales r inner join codpredicados p on r.predicado=p.id where esactivo and sujeto="+id_sujeto);
		Connection connSujeto = data.getConnection(conexion_sujeto);
		Statement stmtSujeto =connSujeto.createStatement();
		
		while (rs.next()){
			String uri_sujeto = rs.getString("uri_sujeto");
			String uri_objeto = rs.getString("uri_objeto");
			ArrayList<String> etiq_sujeto =getEtiquetasUri(uri_sujeto);
			ArrayList<String> etiq_objeto =getEtiquetasUri(uri_objeto);
			String filtro_objeto = rs.getString("filtro_objeto");
			String uri_predicado = rs.getString("uri_predicado");
			if (rs.getString("conexion_objeto").equalsIgnoreCase(conexion_sujeto)){
				log.info("select "+getSelect(etiq_sujeto,"s")+","+getSelect(etiq_objeto,"o")+
						" FROM "+(filtro_sujeto != null && filtro_sujeto.length()>0?"(SELECT * FROM "+capa_sujeto+" WHERE "+filtro_sujeto+") ":capa_sujeto)
							+" s INNER JOIN "+(filtro_objeto != null && filtro_objeto.length()>0?"(SELECT * FROM "+rs.getString("capa_objeto")+
									" WHERE "+filtro_objeto+") ":rs.getString("capa_objeto"))+" o ON "+ getSpatialCondition(rs.getString("predicado"),"s.shape"));
				ResultSet rs_rel = stmtSujeto.executeQuery("select "+getSelect(etiq_sujeto,"s")+","+getSelect(etiq_objeto,"o")+
						" FROM "+(filtro_sujeto != null && filtro_sujeto.length()>0?"(SELECT * FROM "+capa_sujeto+" WHERE "+filtro_sujeto+") ":capa_sujeto)
							+" s INNER JOIN "+(filtro_objeto != null && filtro_objeto.length()>0?"(SELECT * FROM "+rs.getString("capa_objeto")+
									" WHERE "+filtro_objeto+") ":rs.getString("capa_objeto"))+" o ON "+ getSpatialCondition(rs.getString("predicado"),"s.shape"));
				while (rs_rel.next()){
					String ttl ="<"+getUri(uri_sujeto,etiq_sujeto, rs_rel,"s")+">   "+"<"+uri_predicado+">   "+"<"+getUri(uri_objeto,etiq_objeto, rs_rel,"o")+"> .";
					log.info(ttl);
					loadTtl(graph,ttl);
				}
				rs_rel.close();
						
			}
			else{
				log.info("select "+getSelect(etiq_sujeto,"s")+",st_asText(st_transform(shape,4326)) as wkt from "+capa_sujeto+" s"+
						(filtro_sujeto!=null && filtro_sujeto.trim().length()>0?" WHERE "+filtro_sujeto:""));
				ResultSet rs_sujetos = stmtSujeto.executeQuery("select "+getSelect(etiq_sujeto,"s")+",st_asText(st_transform(shape,4326)) as wkt from "+capa_sujeto+" s"+
						(filtro_sujeto!=null && filtro_sujeto.trim().length()>0?" WHERE "+filtro_sujeto:""));
				Connection connObjeto = data.getConnection(rs.getString("conexion_objeto"));
				Statement stmtObjeto =connObjeto.createStatement();
				while (rs_sujetos.next()){
					log.info("select "+getSelect(etiq_objeto,"o")+
							" FROM "+rs.getString("capa_objeto")+" WHERE "+(filtro_objeto!=null && filtro_objeto.trim().length()>0?filtro_objeto+" AND ":"")+
									 getSpatialCondition(rs.getString("predicado"),"'"+rs_sujetos.getString("wkt")+"'"));
					ResultSet rs_rel = stmtObjeto.executeQuery("select "+getSelect(etiq_objeto,"o")+
							" FROM "+rs.getString("capa_objeto")+" WHERE "+(filtro_objeto!=null && filtro_objeto.trim().length()>0?filtro_objeto+" AND ":"")+
									 getSpatialCondition(rs.getString("predicado"),"'"+rs_sujetos.getString("wkt")+"'"));
					while (rs_rel.next()){
						String ttl ="<"+getUri(uri_sujeto,etiq_sujeto, rs_sujetos,"s")+">   <"+uri_predicado+">   <"+getUri(uri_objeto,etiq_objeto, rs_rel,"o")+"> .";
						log.info(ttl);
						loadTtl(graph,ttl);
					}
					rs_rel.close();

				}
				rs_sujetos.close();
				stmtObjeto.close();
				
				
			}
			
			
		}
		stmtSujeto.close();
		
	}
	
	protected String getUri(String patron_uri,ArrayList<String> etiquetas,ResultSet rs,String prefijo) throws Exception{
		String uri = patron_uri;
		for (String etiqueta:etiquetas){
			uri = uri.replaceAll("<<"+etiqueta+">>",rs.getString(prefijo+"_"+etiqueta));
		}
		return uri;
	}
	
	protected String getSpatialCondition(String operacion, String shape){
		 String where = operacion+" ("+shape+",o.shape)";

         if(operacion.equals("Norte")){
             where = "ST_Azimuth(ST_Centroid(o.shape),ST_Centroid("+shape+"))<"+Math.PI/2+" AND ST_Azimuth(ST_Centroid(o.shape),ST_Centroid("+shape+"))>"+(3*Math.PI)/2;
         }else if(operacion.equals("Sur")){
             where = "ST_Azimuth(ST_Centroid(o.shape),ST_Centroid("+shape+"))>"+Math.PI/2+" AND ST_Azimuth(ST_Centroid(o.shape),ST_Centroid("+shape+"))<"+(3*Math.PI)/2;
         }else if(operacion.equals("Este")){
             where = "ST_Azimuth(ST_Centroid(o.shape),ST_Centroid("+shape+")<"+Math.PI;
         }else if(operacion.equals("Oeste")){
             where = "ST_Azimuth(ST_Centroid(o.shape),ST_Centroid("+shape+")>"+Math.PI;
         }else if(operacion.equals("Cerca")){
             where = "ST_Distance(o.shape,"+shape+")<243";
         }else if(operacion.equals("Normal")){
             where = "ST_Distance(o.shape,"+shape+")>243 AND ST_Distance(o.shape,"+shape+")<19683";
         }else if(operacion.equals("Lejos")){
             where = "ST_Distance(o.shape,"+shape+")>19683";
         }else if(operacion.equals("Derecha") || operacion.equals("Izquierda")){
             where = "true";
         }
         return where;
	}
	protected String getSelect(ArrayList<String> campos, String alias){
		String select="";
		for (String campo:campos){
			if (select.length()>0){
				select +=",";
			}
			select+=alias+"."+campo+" as "+alias+"_"+campo;
		}
		return select;
	}
	protected ArrayList<String> getEtiquetasUri(String uri){
		  Pattern pattern = Pattern.compile("<<[^>]*>>", Pattern.CASE_INSENSITIVE);
          Matcher m = pattern.matcher(uri);
          ArrayList<String> etiquetas = new ArrayList<String>();
          while (m.find()) {
              String etiqueta = m.group(m.groupCount());
             etiquetas.add(etiqueta.replaceAll("<<", "").replaceAll(">>", ""));
          }
		return etiquetas;
	}
	protected String cleanTtl(String ttl, String etiqueta){
		log.info("etiqueta "+etiqueta);
		int startIndex = ttl.indexOf(etiqueta);
		//log.info(ttl);
		while (startIndex>=0){
			log.info("startIndex "+startIndex);
			int endIndex = ttl.indexOf(etiqueta)+etiqueta.length();
			int endPoint=ttl.indexOf(".", endIndex);
			int endComa=ttl.indexOf(";", endIndex);
			int endLine;
			if (endPoint>-1 && endComa>-1){
				endLine = Math.min(endComa, endPoint);
			}
			else{
				endLine = (endPoint>-1 ? endPoint:endComa);
			}
			int startPoint=ttl.indexOf(".");
			int startComa=ttl.indexOf(";");
			int startLine=startPoint;
			/*log.info("startIndex"+startIndex);
			log.info("startComa"+startComa);
			log.info("startPoint"+startPoint);
			log.info("startLine"+startLine);*/
			while (startPoint<startIndex && startPoint>-1 ){
				startLine=startPoint;
				//log.info("startPoint"+startPoint);
				startPoint = ttl.indexOf(".",startPoint+1);
			}
			while (startComa<startIndex && startComa>-1){
				startLine=Math.max(startComa,startLine);
				startComa = ttl.indexOf(";",startComa+1);
				//log.info("startComa"+startComa);
			}
			/*log.info("startIndex"+startIndex);
			log.info("startComa"+startComa);
			log.info("startPoint"+startPoint);
			log.info("startLine"+startLine);
			log.info("endIndex"+endIndex);
			log.info("endComa"+endComa);
			log.info("endPoint"+endPoint);
			log.info("endLine"+endLine);*/
			if (endLine==endPoint){
				log.info("finpunto"+ttl.substring(startLine, endLine));
				ttl =  ttl.replaceFirst(Pattern.quote(ttl.substring(startLine, endLine+1)), ".");
			}
			else{
				log.info("fin"+ttl.substring(startLine+1, endLine));
				ttl= ttl.replaceFirst(Pattern.quote(ttl.substring(startLine+1, endLine+1)), "");
			}
			//log.info("ttl "+ttl);
			startIndex = ttl.indexOf(etiqueta);
			log.info("HaymasstartIndex"+startIndex);
		}
		return ttl;
	}

	public void loadTtl(VirtGraph graph, String ttl) throws Exception{
		/*log.info(grafo_virtuoso);
		log.info(url_virtuoso);*/
		log.info(ttl);
		//RDFParserBuilder parser = RDFParser.create().fromString(new String(ttl.getBytes("ISO-8859-1"), "UTF-8"));
		RDFParserBuilder parser = RDFParser.create().fromString(ttl);
		parser.lang(Lang.TTL).parse(graph);
	}
	public void loadTtlFile(String uriOrFile) throws Exception{
		log.info(grafo_virtuoso);
		log.info(url_virtuoso);
		log.info(uriOrFile);
		VirtGraph graph = new VirtGraph(grafo_virtuoso, url_virtuoso, user_virtuoso, pwd_virtuoso);


		RDFParserBuilder parser = RDFParser.create().source(uriOrFile);
		parser.lang(Lang.TTL).parse(graph);
graph.close();
	}
	public void deleteType(String tipo) throws Exception{
		VirtGraph graph = new VirtGraph(grafo_virtuoso, url_virtuoso, user_virtuoso, pwd_virtuoso);
		 String queryString = "DELETE {?s ?p ?o.} FROM <"+grafo_virtuoso+"> WHERE {  ?s a <"+tipo+">; ?p ?o. }";
		 log.debug(queryString);
		VirtuosoUpdateRequest request = VirtuosoUpdateFactory.create(queryString,graph) ;
		request.exec();
		graph.close();
	}

}
