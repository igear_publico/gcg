package  idearium.gcg;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class SolrService {
	@Value("${solr.url}")
	String solr_url;	

	@Value("${sparql.url}")
	String url_sparql;
	@Value("${virtuoso.grafo}")
	String grafo_virtuoso;

	@Value("${opendata.url}")
	String sparql_opendata;

	@Value("${wikidata.url}")
	String sparql_wikidata;
	
	SolrClient solr;
	
	@Autowired
	private DataSourceConfiguration data;
	
	public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");



	private final static Logger log = LoggerFactory.getLogger("gcg");


	/**
	 * Indexa todos los documentos de base de datos
	 * @throws Exception
	 */
	public  void indexGraph() throws Exception{
		//try{


			Connection conGCG = data.getConnection("gcg");
			Statement stmt =conGCG.createStatement();
			java.sql.ResultSet rs = stmt.executeQuery("select tipo,tipo_facet from config_busquedas where indexar order by tipo");
			while (rs.next()){
				try{
				indexType(rs.getString("tipo"),rs.getString("tipo_facet"));
				}
				catch(Exception ex){
					
					
						Statement stmt_upd =conGCG.createStatement();
						stmt_upd.executeUpdate("update config_busquedas set error_indexado='"+ex.getMessage()+"'  where tipo='"+rs.getString("tipo")+"'");
						stmt_upd.close();
					throw ex;
				}
			}
		/*}
		finally{
			data.closeConnections();
		}*/

	}
	public  void indexType(String tipo) throws Exception{
		//try{


			Connection conGCG = data.getConnection("gcg");
			Statement stmt =conGCG.createStatement();
			java.sql.ResultSet rs = stmt.executeQuery("select tipo_facet from config_busquedas where tipo='"+tipo+"'");
			while (rs.next()){
				indexType(tipo,rs.getString("tipo_facet"));
			}
			
		/*}
		finally{
			data.closeConnections();
		}*/

	}
	public  void indexType(String tipo, String tipo_facet) throws Exception{
		solr = new HttpSolrClient.Builder(solr_url).build();
		
		//solr.deleteByQuery("item_type:Entity&fq:"+tipo);
		log.debug("SELECT ?uri (?uri as ?id) (GROUP_CONCAT(?tipo; separator=\"#@#\") as ?tipos) (GROUP_CONCAT(coalesce(?label_tipo,if(?tipo in (<http://www.w3.org/ns/org#Organization>),'Organización',if(?tipo in (<http://www.w3.org/2002/07/owl#Class>),'Concepto',?label_tipo))); separator=\"#@#\") as ?tiposLabel) "+
				"(GROUP_CONCAT(?label; separator=\"#@#\") as ?nombre) (GROUP_CONCAT(?com; separator=\"#@#\") as ?comment) ?codigo  ?prefLabel  (GROUP_CONCAT(?altLab; separator=\"#@#\") as ?altLabel) "+
				" (GROUP_CONCAT(?wikiURI; separator=\"#@#\") as ?wikidataURI)   (GROUP_CONCAT(?spatial; separator=\"#@#\") as ?nuts) (GROUP_CONCAT(?spatial_hydro; separator=\"#@#\") as ?nuts_hydro) (GROUP_CONCAT(?nut_literal; separator=\"#@#\") as ?nut) "+
				"(GROUP_CONCAT(?img; separator=\"#@#\") as ?imgs) (GROUP_CONCAT(?keyword; separator=\"#@#\") as ?keywords)  WHERE {"+
				" GRAPH <"+grafo_virtuoso+">{ ?uri <http://www.w3.org/2000/01/rdf-schema#label> ?label."+
				" FILTER (lang(?label)=\"es\"). "+
				" ?uri a ?tipo."+
				" ?uri a <"+tipo+">."+
				" OPTIONAL { ?uri <http://www.w3.org/2000/01/rdf-schema#comment> ?com.}"+
				" OPTIONAL {?uri <http://purl.org/dc/elements/1.1/identifier> ?codigo.}"+
				" OPTIONAL {?uri <https://www.w3.org/ns/dcat#keyword> ?keyword.}"+
				
				" OPTIONAL {?uri <http://www.w3.org/2004/02/skos/core#prefLabel> ?prefLabel.  FILTER (lang(?prefLabel)=\"es\").}"+
				"OPTIONAL { ?uri <http://www.w3.org/2004/02/skos/core#altLabel> ?altLab.}"+
				"OPTIONAL { ?uri <http://purl.org/dc/terms/spatial> ?spatial."
				+ " filter(regex(?spatial, \"http://opendata.aragon.es/recurso/sector-publico/organizacion/\")).}"+
				"OPTIONAL { ?uri <http://purl.org/dc/terms/spatial> ?spatial_hydro."
				+ " filter(regex(?spatial_hydro, \"http://icearagon.aragon.es/resource/hydro/\")).}"+
				"OPTIONAL { ?uri <http://purl.org/dc/terms/spatial> ?nut_literal."
				+ " filter(isLiteral(?nut_literal)).}"+
				"OPTIONAL { ?uri <http://www.wikidata.org/prop/direct/P18> ?img.}"+
				"OPTIONAL { ?tipo <http://www.w3.org/2000/01/rdf-schema#label> ?label_tipo."+
				"FILTER (?tipo!=<http://www.w3.org/2002/07/owl#NamedIndividual>)."+
					" FILTER (lang(?label_tipo)=\"es\").} }"
					
+ " OPTIONAL {?uri ?p ?wikiURI.  filter(regex(?wikiURI, \"http://www.wikidata.org/entity/\")).}"+
				"}GROUP BY ?uri  ?codigo ?prefLabel");
		Query q =  QueryFactory.create("SELECT ?uri (?uri as ?id) (GROUP_CONCAT(?tipo; separator=\"#@#\") as ?tipos) (GROUP_CONCAT(coalesce(?label_tipo,if(?tipo in (<http://www.w3.org/ns/org#Organization>),'Organización',if(?tipo in (<http://www.w3.org/2002/07/owl#Class>),'Concepto',?label_tipo))); separator=\"#@#\") as ?tiposLabel) "+
				"(GROUP_CONCAT(?label; separator=\"#@#\") as ?nombre) (GROUP_CONCAT(?com; separator=\"#@#\") as ?comment) ?codigo  ?prefLabel  (GROUP_CONCAT(?altLab; separator=\"#@#\") as ?altLabel) "+
				" (GROUP_CONCAT(?wikiURI; separator=\"#@#\") as ?wikidataURI)   (GROUP_CONCAT(?spatial; separator=\"#@#\") as ?nuts) (GROUP_CONCAT(?spatial_hydro; separator=\"#@#\") as ?nuts_hydro) (GROUP_CONCAT(?nut_literal; separator=\"#@#\") as ?nut) "+
				"(GROUP_CONCAT(?img; separator=\"#@#\") as ?imgs) (GROUP_CONCAT(?keyword; separator=\"#@#\") as ?keywords)  WHERE {"+
				" GRAPH <"+grafo_virtuoso+">{ ?uri <http://www.w3.org/2000/01/rdf-schema#label> ?label."+
				" FILTER (lang(?label)=\"es\"). "+
				" ?uri a ?tipo."+
				" ?uri a <"+tipo+">."+
				" OPTIONAL { ?uri <http://www.w3.org/2000/01/rdf-schema#comment> ?com.}"+
				" OPTIONAL {?uri <http://purl.org/dc/elements/1.1/identifier> ?codigo.}"+
				" OPTIONAL {?uri <https://www.w3.org/ns/dcat#keyword> ?keyword.}"+
				
				" OPTIONAL {?uri <http://www.w3.org/2004/02/skos/core#prefLabel> ?prefLabel.  FILTER (lang(?prefLabel)=\"es\").}"+
				"OPTIONAL { ?uri <http://www.w3.org/2004/02/skos/core#altLabel> ?altLab.}"+
				"OPTIONAL { ?uri <http://purl.org/dc/terms/spatial> ?spatial."
				+ " filter(regex(?spatial, \"http://opendata.aragon.es/recurso/sector-publico/organizacion/\")).}"+
				"OPTIONAL { ?uri <http://purl.org/dc/terms/spatial> ?spatial_hydro."
				+ " filter(regex(?spatial_hydro, \"http://icearagon.aragon.es/resource/hydro/\")).}"+
				"OPTIONAL { ?uri <http://purl.org/dc/terms/spatial> ?nut_literal."
				+ " filter(isLiteral(?nut_literal)).}"+
				"OPTIONAL { ?uri <http://www.wikidata.org/prop/direct/P18> ?img.}"+
				"OPTIONAL { ?tipo <http://www.w3.org/2000/01/rdf-schema#label> ?label_tipo."+
				"FILTER (?tipo!=<http://www.w3.org/2002/07/owl#NamedIndividual>)."+
					" FILTER (lang(?label_tipo)=\"es\").} }"
					
+ " OPTIONAL {?uri ?p ?wikiURI.  filter(regex(?wikiURI, \"http://www.wikidata.org/entity/\")).}"+
				"}GROUP BY ?uri  ?codigo ?prefLabel");



		QueryExecution stmt = QueryExecutionFactory.sparqlService(url_sparql, q);

		ResultSet rs =  stmt.execSelect();
	

		while (rs.hasNext()){
			log.debug("fila "+rs.getRowNumber());
			QuerySolution obj =rs.nextSolution();
			SolrInputDocument document =new SolrInputDocument(); 
			// el tipo puede ser multivaluado por lo que hay que comprobar si ya existe el doc y en ese caso incluir los tipos ya registrados
			/*	String docId = obj.getResource("uri").getURI();
				SolrDocument doc_solr = solr.getById(docId);
				if (doc_solr!=null){
					Object tipo = doc_solr.get("tipo");
					document.addField("tipo", tipo);
				}*/
			document.addField("item_type", "Entity");
			
			document.addField("facet_tipo", tipo_facet);
			fillDoc(document,obj);
			if (tipo_facet.equalsIgnoreCase("Unidades administrativas")){
				document.addField("prioridad", 1);
				document.addField("nombre_prioritario", document.getFieldValue("nombre").toString());
			}
			else{
				document.addField("prioridad", 2);
			}
			String docId = document.getFieldValue("id").toString();
			SolrDocument doc_solr = solr.getById(docId);
			if (doc_solr!=null){  // si el doc ya existía reemplazarlo por el nuevo
					updateDoc(docId,document);
				}
				else{
			indexDoc(document);
			}
		}
		stmt.close();
		Connection conGCG = data.getConnection("gcg");
		Statement stmt_upd =conGCG.createStatement();
		stmt_upd.executeUpdate("update config_busquedas set ultimo_indexado=current_timestamp, error_indexado=null where  tipo='"+tipo+"'");
		stmt_upd.close();
	}

	/**
	 * Rellena en el documento las fechas de inicio y fin
	 * @param document documento a rellenar
	 * @param stmt
	 * @param docId identificador del documento
	 * @return documento relleno
	 * @throws Exception
	 */
	/*public static SolrInputDocument fillDate(SolrInputDocument document,Statement stmt, String docId) throws Exception{
		String query = "SELECT "+
				" JXFPFEIN as fecha_inicio,[Hora_Inicio]"+
				", JXFPFEFI as fecha_fin,[Hora_Fin]"+
				" FROM [DBJX].[DB2DESA].[TBJX4FIF]"+
				" WHERE JXFPNXXX= "+docId+" AND JXFPNVAR=0 AND JXFPESTA=0";
		logger.debug(query);
		ResultSet rs = stmt.executeQuery(query);
		if (rs.next()){
			if ((rs.getDate("fecha_inicio")!=null)&&(rs.getString("Hora_Inicio")!=null)){
				String fecha_inicio = format.format(rs.getDate("fecha_inicio"))+" "+rs.getString("Hora_Inicio")+"Z";
				document.addField("PLAZO_INICIO_DATE", fecha_inicio);
			}
			if ((rs.getDate("fecha_fin")!=null)&&(rs.getString("Hora_fin")!=null)){
				String fecha_fin = format.format(rs.getDate("fecha_fin"))+" "+rs.getString("Hora_Fin")+"Z";
				document.addField("PLAZO_FIN_DATE", fecha_fin);
			}

		}
		return document;
	}*/

 public boolean isIncluded(String[] vector, int index, String value){
	 for (int i=0;i<index;i++){
		 if (vector[i].equalsIgnoreCase(value)){
			 return true;
		 }
	 }
	 return false;
 }

	/**
	 * Incluye en el documento Solr todos los campos del registro actual del resultSet
	 * @param documento documento Solr
	 * @param rs ResultSet con los datos a incluir en el documento
	 * @return documento original incluyendo los campos del ResultSet
	 * @throws Exception
	 */
 public SolrInputDocument fillDoc(SolrInputDocument document,QuerySolution rs) throws Exception{
	 Iterator<String> vars = rs.varNames();
	 ArrayList<String> nombre = new ArrayList<String>();
	 String prefLabel=null;
	 ArrayList<String> altLabel = new ArrayList<String>();
	 while(vars.hasNext()){
		 String var = vars.next();
		 //log.debug(var);

		 if(var.equalsIgnoreCase("tipos")||var.equalsIgnoreCase("tiposLabel")){
			 if (rs.get(var).isLiteral()){
				 String[] tipos = rs.getLiteral(var).getString().split("#@#");

				 for (int i=0;i<tipos.length;i++){
					 String tipo= tipos[i];
					 if (tipo.length()>0 && !tipo.equalsIgnoreCase("http://www.w3.org/2002/07/owl#NamedIndividual") && !isIncluded(tipos,i,tipo)){

						 document.addField(var, tipo);
					 }
				 }
			 }

			 else{
				 Resource tipo = rs.getResource(var);
				 String valor =	tipo.getURI();
				 document.addField(var, valor);
			 }
		 }
		 else if (var.equals("imgs")){
			 if (rs.get(var).isLiteral()){
			//	 log.debug("imgs:"+rs.getLiteral(var).getString());
				 String[] imgs = rs.getLiteral(var).getString().split("#@#");
				 for (String img:imgs){
					 if (img.length()>0 ){
						 // TODO: seleccionar qué imagen indexar o cambiar a multivaluado 
						 document.addField("img_contenturl", img);
						 break;
					 }
				 }
			 }
			 else{
				 Resource tipo = rs.getResource(var);
				 String valor =	tipo.getURI();
				 document.addField("img_contenturl", valor);
			 }

		 }
		 else if (var.equals("prefLabel")){
			 prefLabel = rs.getLiteral(var).getString();
			 document.addField(var, prefLabel);
		 }
		 else if (var.equals("nombre")||var.equals("altLabel")){

			 String[] nombres = rs.getLiteral(var).getString().split("#@#");
			 for (String label:nombres){
				 if (var.equals("nombre")){
					 nombre.add(label);
				 }
				 else{
					 altLabel.add(label);
					 document.addField(var, label);

				 }
			 }


		 }
		 else if (var.equalsIgnoreCase("nuts")||(var.equalsIgnoreCase("nuts_hydro"))){
			 if (rs.get(var).isLiteral()){

				 String nuts = rs.getLiteral(var).getString();
			//	 log.debug("nuts literal "+nuts);
				 if ((nuts!=null)&&(nuts.trim().length()>0)&&(!nuts.trim().equalsIgnoreCase("#@#"))){
					 nuts = nuts.replaceAll("#@#", ">,<");
					 /*if (var.equalsIgnoreCase("nuts")){
					fillNuts(document,"http://purl.org/dc/elements/1.1/title", sparql_opendata,"<"+nuts+">");
					}
					else{*/
					 fillNuts(document,"http://www.w3.org/2000/01/rdf-schema#label", url_sparql,"<"+nuts+">");
					 //}
				 }
			 }
			 else{
				 Resource tipo = rs.getResource(var);
				 String  valor =	tipo.getURI();
				 fillNuts(document,"http://www.w3.org/2000/01/rdf-schema#label", url_sparql,"<"+valor+">");
			 }
		 }

		 else if (var.equalsIgnoreCase("wikidataURI")){
			 if (rs.get(var).isLiteral()){
				 String value =  rs.getLiteral(var).getString();
				 if (value.indexOf("#@#")>=0){
					 String[] kws = value.split("#@#");
					 for (String kw:kws){
						 if (kw.length()>0 ){
							 if ( fillImage(document,kw)){
								 break;
							 }
						 }
					 }
				 }
				 else{
					 document.addField(var, value);
				 }
			 }
			 else{
				 fillImage(document,rs.getResource(var).getURI());
			 }

		 }
		 else  if (rs.get(var).isLiteral()){
			 String value =  rs.getLiteral(var).getString();
			 if (value.indexOf("#@#")>=0){
				 String[] kws = value.split("#@#");
				 for (int i=0; i<kws.length;i++){
					 String kw=kws[i];
					 if (kw.length()>0 && !isIncluded(kws,i,kw)){

						 document.addField(var, kw);

					 }
				 }
			 }
			 else{
				 document.addField(var, value);
			 }
		 }
		 else{
			
			 Resource tipo = rs.getResource(var);
			 String valor =	tipo.getURI();
			 log.debug(var+":"+valor);
			 document.addField(var, valor);
		 }

	 }
	 if (nombre.size()==1){
		 document.addField("nombre", nombre.get(0));
	 }
	 else{
		 
		 if (prefLabel != null){
			 nombre.remove(prefLabel);
			 document.addField("nombre", prefLabel);	 
		 }
		 else if (nombre.size()>0){
			 nombre.remove(0);
			 document.addField("nombre", nombre.get(0));	 
		 }
		 for (String label:nombre){
			 if (!altLabel.contains(label)){
				 document.addField("altLabel", label);
			 }
		 }
	 }
	
	 
 
 return document;
}

	protected boolean fillImage(SolrInputDocument doc, String uri) throws Exception{
		String id = uri.replaceAll("http://www.wikidata.org/entity/", "");
		String query ="SELECT ?img ?urlES ?urlEN ?url "+
		" WHERE "+
		" {"+
		" wd:"+id+" wdt:P18 ?img."+
		" OPTIONAL {"+
		" ?urlES schema:about wd:"+id+"."+
		" ?urlES schema:inLanguage \"es\"."+
		" }"+
		" OPTIONAL {"+
		" ?urlEN schema:about wd:"+id+"."+
		" ?urlEN schema:inLanguage \"en\"."+
		" }"+
		" ?url schema:about wd:"+id+"."+
		" ?url schema:inLanguage ?lng."+
		" }"+
		"order by ?lng"+
		" limit 1";
		log.debug(query);
		String request =sparql_wikidata+"?query="+URLEncoder.encode(query,"UTF-8");
			try{
		URL url = new URL(request);

		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.setRequestProperty("Accept", "application/json");

		connection.connect();
		int code = connection.getResponseCode();



		if (code==200){
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}
			String response  = sb.toString();
			log.info(response);
			connection.disconnect();
			if (sb.length()>0){
				try{
					JsonObject json= new JsonParser().parse(response).getAsJsonObject();
					JsonElement vars = json.getAsJsonObject("results").getAsJsonArray("bindings").get(0);
					JsonElement img =vars.getAsJsonObject().get("img");
					doc.addField("img_contenturl", img.getAsJsonObject().get("value").getAsString());
					doc.addField("img_license", "https://creativecommons.org/publicdomain/zero/1.0/");
					JsonElement img_url =vars.getAsJsonObject().get("urlES");
					if (img_url==null){
						log.info("No hay url en español");
						img_url =vars.getAsJsonObject().get("urlEN");
						if (img_url==null){
							log.info("No hay url en inglés");
							img_url =vars.getAsJsonObject().get("url");
						}

					}
					if (img_url != null){
						doc.addField("img_url", img_url.getAsJsonObject().get("value").getAsString());
					}
				}
				catch(Exception ex){
					log.warn("No localizada foto en wikidata "+request,ex);
					return false;
				}
			}
		}
		else{
			log.error("Error obteniendo imagen de wikidata ("+request+"). Respuesta:"+code+"-"+(connection.getResponseMessage()!=null? connection.getResponseMessage():""));
			throw new Exception();
		}
		}
		catch(Exception ex){
			log.error("Error obteniendo imagen de wikidata ("+request+")",ex);
			return false;
		}
			return true;
	}
	
	protected void fillNuts(SolrInputDocument doc, String predicado_nombre, String sparql_url, String nuts) throws Exception{
		
		String query ="SELECT ?nut "+
		" WHERE { "+
		" ?s <"+predicado_nombre+"> ?nut."+
		" FILTER (?s in ("+nuts+"))."+
			"} ";
		
		//log.debug(query);
		String request =sparql_url+"?query="+URLEncoder.encode(query,"UTF-8");
			try{
		URL url = new URL(request);

		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.setRequestProperty("Accept", "application/json");

		connection.connect();
		int code = connection.getResponseCode();



		if (code==200){
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}
			String response  = sb.toString();
			//log.info(response);
			connection.disconnect();
			if (sb.length()>0){
				
					JsonObject json= new JsonParser().parse(response).getAsJsonObject();
					JsonArray vars = json.getAsJsonObject("results").getAsJsonArray("bindings");
					String[] incluidos=new String[vars.size()];
					for (int i=0; i<vars.size();i++){
						JsonElement nut =vars.get(i).getAsJsonObject().get("nut");
						String valor=nut.getAsJsonObject().get("value").getAsString();
						if( !isIncluded(incluidos,i,valor)){
								doc.addField("nut",valor);
							incluidos[i]=valor;
						}
					}
				
			}
		}
		else{
			log.error("Error obteniendo nombres de spatial de AOD ("+request+"). Respuesta:"+code+"-"+(connection.getResponseMessage()!=null? connection.getResponseMessage():""));
			throw new Exception();
		}
		}
		catch(Exception ex){
			log.error("Error obteniendo datos de AOD ("+request+")",ex);
		}
	}
	/**
	 * Indexa un documento
	 * @param document documento a indexar en Solr
	 * @throws Exception
	 */
	public  void indexDoc(SolrInputDocument document) throws Exception{
		UpdateResponse response = solr.add(document);
		response.getStatus();
		solr.commit();
	}

	/**
	 * Actualiza un documento en Solr (si no existe lo crea)
	 * @param id identificador del documento
	 * @param doc documento Solr a actualizar
	 * @throws Exception
	 */
	public  void updateDoc(String id, SolrInputDocument doc) throws Exception{
		deleteDoc(id,false);
		indexDoc(doc);
	}

	/**
	 * Elimina un documento de Solr de forma definitiva (commit=true) o provisional (commit=false)
	 * @param id identificador del documento a eliminar
	 * @param commit realizar el commit del borrado o no
	 * @throws Exception
	 */
	public  void deleteDoc(String id, boolean commit) throws Exception{
		solr.deleteById(id);
		solr.commit();
	}

	/**
	 * Elimina un documento de Solr
	 * @param id identificador del documento a eliminar
	 * @throws Exception
	 */
	public  void deleteDoc(String id) throws Exception{
		deleteDoc(id,true);
	}
}
