# gcg



### Dependencias

Para el funcionamiento del servicio gcg es necesario disponer de:

- tablas de configuración, según el modelo ER de la carpeta doc, almacenadas en PostgreSQL
- Virtuoso para almacenar el grafo
- Solr en el que indexar los objetos del grafo



### Instalación

Para instalarlo, generar el war y desplegar en el servidor de aplicaciones
